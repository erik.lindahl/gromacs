**Other Issue**

(Only use this template when no other issue type fits.)

**Description**

(Provide the same type of information we request for other issues. Why is this important and should it be prioritized over other issues? If you are a developer, do you think there's a risk we will just end up closing this as a good idea we didn't have time to work on in three months? Then it might be better to avoid opening it.) 

/label ~Other-Issue


